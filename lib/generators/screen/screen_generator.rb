class ScreenGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('templates', __dir__)
  
  def screen_template
    template "screen.html.erb", "app/views/screens/#{file_name}.html.erb" 
  end
end
