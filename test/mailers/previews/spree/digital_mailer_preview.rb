# Preview all emails at http://localhost:3000/rails/mailers/spree/digital_mailer
class Spree::DigitalMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/spree/digital_mailer/download_link
  def download_link
    Spree::DigitalMailer.download_link
  end

end
