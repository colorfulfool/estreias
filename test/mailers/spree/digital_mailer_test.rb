require "test_helper"

class Spree::DigitalMailerTest < ActionMailer::TestCase
  setup do
    @store = FactoryBot.create(:store, name: "Estreias")
    @product = FactoryBot.create(:product, name: "Celebration by Sheezy")

    @digital_download = FactoryBot.create(:digital)
    @product.master.digitals = [@digital_download]

    @order = FactoryBot.create(:order, 
      line_items: [FactoryBot.create(:line_item, product: @product)])
  end

  test "download_link" do
    mail = Spree::DigitalMailer.download_link(@order)

    assert_equal "Estreias: Celebration by Sheezy", mail.subject
    assert_match @product.name, mail.body.encoded
  end
end
