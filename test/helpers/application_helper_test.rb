require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "clipped" do
    skip
    assert_equal %{<div style="width: 100%; padding-bottom: 56.25%; background: url('https://img.youtube.com/vi/NRRmDNZpo6w/hqdefault.jpg'); background-size: cover; background-position: center;">},
      clipped("https://img.youtube.com/vi/NRRmDNZpo6w/hqdefault.jpg", aspect_ratio: 16/9.to_f)
  end
end