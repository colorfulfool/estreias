require 'test_helper'

class ScreensControllerTest < ActionDispatch::IntegrationTest
  setup do
    @artist = Artist.create(name: "Abdul")
  end
  
  test "screen" do
    get "/artists"
    assert_response :success
  end
  
  test "screen with param" do
    get "/artists/#{@artist.slug}"
    assert_response :success
  end
  
  test "request for non-existent screen returns 404" do
    get "/sitemap.xml"
    assert_response :not_found
    
    get "/admin/config.php"
    assert_response :not_found
  end
end
