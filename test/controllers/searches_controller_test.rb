require 'test_helper'

class SearchesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @record = Artist.create(name: "Abdul")
  end
  
  test "exiting record" do
    get search_path(query: @record.name), xhr: true
    assert response.body.include? @record.name
  end
  
  test "nonexistent record" do
    get search_path(query: "Bell"), xhr: true
    assert response.body.include? "Content not found"
  end
end
