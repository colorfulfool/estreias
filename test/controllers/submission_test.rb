require 'test_helper'

class SubmissionTest < ActionDispatch::IntegrationTest
  test "video" do
    link = "https://www.youtube.com/watch?v=KhTrpnXlOsA"
    
    assert_difference "Video.count", +1 do
      post videos_path, params: {
        video: {youtube: link}
      }
      assert_response :success
      assert_match /Thank you/, response.body
    end
    
    Video.last.tap do |video|
      assert_equal link, video.youtube
      refute video.published
    end
  end
  
  test "validation errors" do
    post videos_path, params: {
      video: {youtube: ""}
    }
    assert_response :success
    refute_match /Thank you/, response.body, "Appears to accept an invalid submission"
    assert_match /blank/, response.body
  end
end
