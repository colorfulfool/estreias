require 'test_helper'

class VideoTest < ActiveSupport::TestCase
  test "YouTube id from normal link" do
    video = Video.new(youtube: "https://www.youtube.com/watch?v=KhTrpnXlOsA")
    assert_match "KhTrpnXlOsA", video.send(:youtube_id)
  end
  
  test "YouTube id from messed-up link" do
    video = Video.new(youtube: "https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=2ahUKEwito7Gnte_kAhWI1aYKHSJ_BK0QyCkwAHoECAkQBQ&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DBRogfZvEfJQ&usg=AOvVaw2O3Huih0_qx9PAq2OiDynh")
    assert_match "BRogfZvEfJQ", video.send(:youtube_id)
  end
end


