require 'test_helper'

class SoundCloudTest < ActiveSupport::TestCase
  test "song with song" do
    song = Song.new(soundcloud: "https://soundcloud.com/mukubalrecords/bomba-ompuff-prodnedbeat-mukubal")
    assert_match "329418511", song.send(:soundcloud_id)
    assert_match "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/329418511&color=%23ff3160&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true&buying=false",
      song.embed_url
  end
  
  test "playlist with playlist" do
    playlist = Playlist.new(soundcloud: "https://soundcloud.com/marlon-brandon-mb/sets/in-the-house")
    assert_match "321014523", playlist.send(:soundcloud_id)
    assert_match "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/321014523&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true",
      playlist.embed_url
  end
  
  test "song with playlist" do
    song = Song.new(soundcloud: "https://soundcloud.com/marlon-brandon-mb/sets/in-the-house")
    assert_match "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/321014523&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true",
      song.embed_url
  end
end
