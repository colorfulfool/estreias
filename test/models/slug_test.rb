require 'test_helper'

class SlugTest < ActiveSupport::TestCase
  test "slug generated" do
    artist = Artist.create(name: "Shmartist No 1")
    assert_match "shmartist-no-1", artist.slug
  end
end
