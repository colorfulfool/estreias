require "application_system_test_case"

class CheckoutTest < ApplicationSystemTestCase
  setup do
    @store = FactoryBot.create(:store)
    
    @shipping_category = FactoryBot.create(:shipping_category)
    @shipping_method = FactoryBot.create(:shipping_method, 
      name: "Free Shipping",
      shipping_categories: [@shipping_category], 
      calculator: Spree::BasicShippingCalculator.new)
    
    @payment_method = Spree::BasicPaymentMethod.create(
      name: "Bank Deposit",
      preferred_instuctions_in_english: "Text me %{order_number}")
    
    @product = FactoryBot.create(:product, shipping_category: @shipping_category)
  end
  
  test "digital product" do
    @digital_download = FactoryBot.create(:digital)
    @product.master.digitals = [@digital_download]

    visit "/shop/products/#{@product.slug}"
    
    click_on "Buy Now"
    
    assert_checkout "Address" do
      assert_selector "#summary-order-total", text: "$19.99"
      
      fill_in "Email", with: customer_email
      
      fill_in_address
      
      click_on "Save and Continue"
    end
    
    assert_checkout "Delivery" do
      assert_selector ".rate-name", text: @shipping_method.name
      assert_selector "[data-hook='shipping-total']", text: "$0.00"
      
      click_on "Save and Continue"
    end
    
    assert_checkout "Payment" do
      assert_text /Text me R\d+/
      
      click_on "Confirm Order"
    end

    assert_current_path "/shop/"
    
    assert_enqueued_emails 0
    
    Spree::Order.last.tap do |order|
      assert_equal customer_email, order.email
      order.payments.first.capture!
    end
    
    assert_enqueued_emails 1
  end
  
  test "physical product, can't ship" do
    visit "/shop/products/#{@product.slug}"
    
    click_on "Buy Now"
    
    assert_checkout "Address" do
      assert_selector "#summary-order-total", text: "$19.99"
      
      fill_in "Email", with: customer_email
      
      fill_in_address(city: "Malanje")
      
      click_on "Save and Continue"
    end
    
    assert_text "please contact us directly"
  end
  
  private

    def customer_email
      "anton@home.com"
    end
  
    def assert_checkout(step)
      assert_selector "h1", text: "CHECKOUT"
      # assert_selector ".active", text: step
      yield
    end
    
    def fill_in_address(city: nil)
      fill_in "First Name", with: "Johan"
      fill_in "Last Name", with: "Johannovich"
      fill_in "Address", with: "1535 Cost Avenue"
      fill_in "City", with: (city || "Luanda")
      fill_in "Zip Code", with: "20783"
      select "Angola", from: "Country"
      fill_in "Phone", with: "301-466-5774"
    end
end
