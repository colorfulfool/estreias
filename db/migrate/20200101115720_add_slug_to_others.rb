class AddSlugToOthers < ActiveRecord::Migration[6.0]
  def change
    add_column :songs, :slug, :string
    add_column :movies, :slug, :string
    add_column :videos, :slug, :string
    
    # generate slugs for existing records
    reversible do |direction|
      direction.up do
        [Song, Movie, Video].each do |model|
          model.find_each(&:save!)
        end  
      end
    end
  end
end
