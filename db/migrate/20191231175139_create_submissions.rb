class CreateSubmissions < ActiveRecord::Migration[6.0]
  def change
    create_table :submissions do |t|
      t.string :kind
      t.string :soundcloud
      t.string :youtube
      t.text :lyrics

      t.timestamps
    end
  end
end
