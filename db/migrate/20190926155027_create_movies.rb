class CreateMovies < ActiveRecord::Migration[6.0]
  def change
    create_table :movies do |t|
      t.date :starts
      t.date :ends

      t.timestamps
    end
  end
end
