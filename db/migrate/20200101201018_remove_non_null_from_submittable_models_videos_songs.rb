class RemoveNonNullFromSubmittableModelsVideosSongs < ActiveRecord::Migration[6.0]
  def change
    change_column :videos, :artist_id, :bigint, null: true
    change_column :songs, :artist_id, :bigint, null: true
  end
end
