class AddSlugToAlbumGenrePlaylist < ActiveRecord::Migration[6.0]
  def change
    add_column :genres, :slug, :string
    add_column :playlists, :slug, :string
    add_column :albums, :slug, :string

    # generate slugs for existing records
    reversible do |direction|
      direction.up do
        [Genre, Playlist, Album].each do |model|
          model.find_each(&:save!)
        end  
      end
    end
  end
end
