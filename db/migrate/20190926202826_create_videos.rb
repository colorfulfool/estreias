class CreateVideos < ActiveRecord::Migration[6.0]
  def change
    create_table :videos do |t|
      t.belongs_to :artist, null: false, foreign_key: true
      t.string :title
      t.string :youtube

      t.timestamps
    end
  end
end
