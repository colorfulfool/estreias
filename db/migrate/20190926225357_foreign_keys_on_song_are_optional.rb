class ForeignKeysOnSongAreOptional < ActiveRecord::Migration[6.0]
  def change
    change_column_null :songs, :artist_id, true
    change_column_null :songs, :album_id, true
  end
end
