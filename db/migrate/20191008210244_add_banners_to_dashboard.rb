class AddBannersToDashboard < ActiveRecord::Migration[6.0]
  def change
    banner_slots = [:genre_banner, :videos_banner, :movie_banner, :playlist_banner, 
     :movies_banner, :artist_banner, :album_banner, :contact_banner, 
     :song_banner, :video_banner, :playlists_banner, :home_banner, 
     :shop_banner, :genres_banner, :artists_banner]
     
    banner_slots.each do |association|
      add_column :dashboards, "#{association}_id", :integer
    end
  end
end
