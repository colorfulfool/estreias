class RemoveBannerFromDashboard < ActiveRecord::Migration[6.0]
  def change
    remove_column :dashboards, :banner, :text
  end
end
