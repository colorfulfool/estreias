class AddPublishedFlagToSongsVideos < ActiveRecord::Migration[6.0]
  def change
    add_column :songs, :published, :boolean, default: false
    add_column :videos, :published, :boolean, default: false
  end
end
