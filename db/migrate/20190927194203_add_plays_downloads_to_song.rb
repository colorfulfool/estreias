class AddPlaysDownloadsToSong < ActiveRecord::Migration[6.0]
  def change
    add_column :songs, :plays, :integer, default: 0
    add_column :songs, :downloads, :integer, default: 0
  end
end
