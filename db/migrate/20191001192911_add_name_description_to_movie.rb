class AddNameDescriptionToMovie < ActiveRecord::Migration[6.0]
  def change
    remove_column :movies, :link, :string
    
    add_column :movies, :name, :string
    add_column :movies, :youtube, :string
    add_column :movies, :description, :text
    add_column :movies, :showings_description, :string
  end
end
