class AddExtraHtmlToDashboard < ActiveRecord::Migration[6.0]
  def change
    add_column :dashboards, :extra_html, :text
  end
end
