class RenameTitleColumns < ActiveRecord::Migration[6.0]
  def change
    [:albums, :songs, :videos].each do |table|
      rename_column table, :title, :name
    end
  end
end
