class AddSoundcloudToPlaylist < ActiveRecord::Migration[6.0]
  def change
    add_column :playlists, :soundcloud, :string
  end
end
