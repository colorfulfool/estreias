class CreateDashboards < ActiveRecord::Migration[6.0]
  def change
    create_table :dashboards do |t|
      t.text :banner
      t.boolean :cinema_hot
      t.belongs_to :featured_song

      t.timestamps
    end
  end
end
