module ApplicationHelper
  def clip_to_aspect_ratio(image, aspect_ratio, html_class: "")
    percentage = 1/aspect_ratio * 100
    style = "width: 100%; padding-bottom: #{percentage}%; 
      background: url('#{url_for image}'); background-size: cover; background-position: center;"
    content_tag(:div, nil, style: style, class: html_class)
  end
  
  
  def current_path_under? path
    request.path.starts_with? path
  end
  
  
  def dashboard
    Dashboard.first || Dashboard.new
  end
  
  def featured_video
    dashboard.featured_song
  end
  
  
  def banner(slot = params[:screen_name])
    render "application/banner", banner: dashboard.banner_for(slot)
  end
end
