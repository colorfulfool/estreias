module SeoHelper
  def title
    [
      content_for(:title),
      content_for(:heading) || current_record&.name,
      content_for(:subheading),
      "ESTREIAS"
    ].compact.join(" | ")
  end
  
  private
  
    def current_record
      @song || @video || @album || @artist || @movie
    end
end