ActiveAdmin.register Dashboard do
  menu false
  
  form title: "Website Settings" do |f|
    inputs "Masthead" do
      input :cinema_hot, hint: "Mark Cinema section with a fire emoji."
      input :featured_song, label: "Featured video", hint: "Shows up on the masthead. Thumbnail becomes the masthead background."
    end
    
    inputs "Banners" do
      Dashboard.slots.each do |slot|
        input slot, label: slot.to_s.remove("_banner").humanize
      end
    end
    
    inputs "Third-party Services" do
      input :extra_html, hint: "Any HTML specified here will be insterted into <head> of every page."
    end
    
    actions do
      action :submit, label: "Save changes"
    end
  end
  
  controller do
    def permitted_params
      params.permit!
    end
  end
end
