ActiveAdmin.register Song do
  permit_params :name, :artist_id, :album_id, :image, :soundcloud, 
    :audio_file, :lyrics, :published, :genre_ids => []
    
  index do
    selectable_column
    column :name
    column :artist
    column :album
    column :published
    column :created_at
    actions
  end
  
  filter :name
  filter :artist
  filter :published
  
  config.sort_order = "created_at_desc"
  
  form do |f|
    f.object.published = true if f.object.new_record?
    
    inputs do
      input :name
      input :artist
      input :album
      input :genres
      input :published
      input :image, as: :file, hint: (image_tag(f.object.image, height: 200) if f.object.image.present?)
    end
      
    inputs do
      input :soundcloud
      input :audio_file, as: :file, hint: (f.object.audio_file.to_s if f.object.audio_file.present?)
    end
    
    inputs do
      input :lyrics
    end
    actions
  end

  controller do
    def create
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end
  end
end
