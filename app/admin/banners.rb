ActiveAdmin.register Banner do
  permit_params :name, :image, :mobile_image, :link

  index do
    selectable_column
    column :name
    column :image do |banner|
      image_tag banner.image, width: 50
    end
    column :created_at
    actions
  end
  
  filter :name
  
  config.sort_order = "created_at_desc"
  
  recommendation = "Recommended: rectangular image, as least 1400x170"
  mobile_recommendation = "Recommended: nearly squale image, at least 650x550"

  form do |f|
    inputs do
      input :name
      input :image, as: :file, hint: (f.object.image.present? ? image_tag(f.object.image.variant(resize_to_limit: [300, 100])) : "") + "\n" + recommendation
      input :link
      input :mobile_image, as: :file, hint: (f.object.mobile_image.present? ? image_tag(f.object.mobile_image.variant(resize_to_limit: [300, 100])) : "") + "\n" + mobile_recommendation
    end
    actions
  end

  controller do
    def create
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end
  end
end
