ActiveAdmin.register Playlist do
  permit_params :name, :soundcloud, :song_ids => []
  
  index do
    selectable_column
    column :name
    column :created_at
    actions
  end
  
  config.sort_order = "created_at_desc"

  filter :name
  filter :songs
  
  form do |f|
    inputs do
      input :name
      input :soundcloud
      input :songs
    end
    actions
  end
end
