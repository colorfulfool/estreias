ActiveAdmin.register Album do
  permit_params :name, :artist_id, :cover, :song_ids => []
  
  index do
    selectable_column
    column :cover do |album|
      image_tag album.cover, height: 50 if album.cover.present?
    end
    column :name
    column :artist
    column :created_at
    actions
  end
  
  filter :name
  filter :artist
  filter :songs
  
  config.sort_order = "created_at_desc"

  form do |f|
    inputs do
      input :name
      input :artist
      input :cover, as: :file, hint: (image_tag(f.object.cover, height: 200) if f.object.cover.present?)
      input :songs
    end
    actions
  end
  
  controller do
    def create
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end
  end
end
