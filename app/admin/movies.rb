ActiveAdmin.register Movie do
  permit_params :starts, :ends, :poster, :name, :description, :showings_description, :youtube
  
  index do
    selectable_column
    column :poster do |album|
      image_tag album.poster, height: 50 if album.poster.present?
    end
    column :created_at
    actions
  end
  
  filter :name
  filter :starts
  
  config.sort_order = "created_at_desc"
  
  form do |f|
    f.object.starts ||= Date.today + 10.days
    # f.object.ends ||= Date.today + 30.days
    
    inputs do
      input :poster, as: :file, hint: (image_tag(f.object.poster, height: 200) if f.object.poster.present?)
      input :starts
    end
    
    inputs do
      input :name
      input :description, as: :text
      input :showings_description, label: "Showing at"
      input :youtube
    end
    actions
  end
  
  controller do
    def create
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end
  end
end
