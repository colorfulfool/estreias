ActiveAdmin.register Video do
  permit_params :artist_id, :name, :youtube, :published
  
  index do
    selectable_column
    column :name
    column :artist
    column :published
    column :created_at
    actions
  end
  
  filter :name
  filter :artist
  filter :published
  
  config.sort_order = "created_at_desc"
  
  form do |f|
    f.object.published = true if f.object.new_record?
    
    inputs do
      input :name
      input :artist
      input :youtube
      input :published
    end
      
    actions
  end

  controller do
    def create
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end
  end
end
