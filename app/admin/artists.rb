ActiveAdmin.register Artist do
  permit_params :name, :image, :bio

  index do
    selectable_column
    column :name
    actions
  end
  
  filter :name  
  
  form do |f|
    inputs do
      input :name
      input :image, as: :file, hint: (image_tag(f.object.image, height: 200) if f.object.image.present?)
      input :bio
    end
    
    actions
  end
end
