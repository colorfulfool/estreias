class Banner < ApplicationRecord
  has_one_attached :image
  has_one_attached :mobile_image
  validates_presence_of :name, :image
end
