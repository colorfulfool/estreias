class Movie < ApplicationRecord
  has_one_attached :poster
  
  scope :showing, -> { order(:starts => :asc) }
  scope :upcoming, -> { order(:starts => :asc) }
  
  include Youtube, Slug
  
  validates_presence_of :poster, :starts
end
