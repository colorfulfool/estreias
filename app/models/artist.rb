class Artist < ApplicationRecord
  has_many :albums
  has_many :songs
  
  has_many :videos
  
  has_one_attached :image
  
  has_and_belongs_to_many :genres
  
  include Slug
end
