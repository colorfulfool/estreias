class Dashboard < ApplicationRecord
  belongs_to :featured_song, class_name: "Video"
  
  include Banners
end
