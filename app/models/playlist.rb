class Playlist < ApplicationRecord
  has_and_belongs_to_many :songs
  
  include Slug
  
  include Soundcloud
end
