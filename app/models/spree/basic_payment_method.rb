module Spree
  class BasicPaymentMethod < PaymentMethod::Check
    preference :instuctions_in_english, :text
    preference :instuctions_in_portugese, :text
  end
end