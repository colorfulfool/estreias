module Spree::StateChangeDecorator
  def self.prepended(base)
    base.after_create :send_digital_download_link
  end
  
  private
  
    def send_digital_download_link
      if order&.some_digital? && next_state == "completed"
        Spree::DigitalMailer.download_link(order).deliver_later
      end
    end
    
    def order
      stateful.order if stateful.is_a?(Spree::Payment)
    end
end

Spree::StateChange.prepend(Spree::StateChangeDecorator)