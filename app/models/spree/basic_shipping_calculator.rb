module Spree
  class BasicShippingCalculator < ShippingCalculator
    def self.description
      "Free Shipping to Luanda and Digital Products"
    end

    def compute(package)
      0
    end

    def available? package
      ships_to_luanda?(package) || digital_product?(package)
    end

    private

      def ships_to_luanda? package
        package.order.ship_address.city == "Luanda"
      end

      def digital_product? package
        package.order.digital?
      end
  end
end