module Submittable
  extend ActiveSupport::Concern
  
  included do
    scope :published, -> { where(published: true) }
  end
end