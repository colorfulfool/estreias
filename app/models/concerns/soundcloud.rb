require "open-uri"

module Soundcloud
  def embed_url
    if soundcloud.include? "sets"
      "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/#{soundcloud_id}&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"
    else
      "https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/#{soundcloud_id}&color=%23ff3160&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true&buying=false"
    end
  end
  
  private
  
    def soundcloud_id
      Nokogiri::HTML(open(soundcloud))
        .css('[property="al:ios:url"]').first
        .attr("content").split(":").last
    end
end