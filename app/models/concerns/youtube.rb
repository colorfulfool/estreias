module Youtube
  def highres_thumbnail
    "https://img.youtube.com/vi/#{youtube_id}/maxresdefault.jpg"
  end
  
  def thumbnail
    "https://img.youtube.com/vi/#{youtube_id}/hqdefault.jpg"
  end
  
  def embed_url(autoplay: false)
    "https://www.youtube.com/embed/#{youtube_id}?autoplay=#{autoplay ? 1 : 0}"
  end

  private
  
    def youtube_id
      youtube.scan(/v(=|%3D)(\w+)/).flatten.second
    end
end