module Banners
  extend ActiveSupport::Concern
  
  included do
    SCREENS_WITHOUT_BANNERS = ["submission"]
  
    def self.slots
      screens = Rails.root.join("app/views/screens").glob("*")
        .map{ |template| template.basename(".html.erb").to_s }
        
      screens_with_banners = screens - SCREENS_WITHOUT_BANNERS
      
      screens_with_banners.map do |screen_name|
        "#{screen_name}_banner".to_sym
      end
    end
    
    slots.each do |slot|
      belongs_to slot, class_name: "Banner", optional: true
    end
  end
  
  def banner_for(slot)
    send("#{slot}_banner".to_sym)
  end
end