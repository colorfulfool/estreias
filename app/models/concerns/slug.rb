module Slug
  extend ActiveSupport::Concern
  
  included do
    before_save do
      self.slug = sluggable
    end
  end
  
  private
  
    def sluggable
      base = name || created_at.to_s
      base += " #{artist.name}" if (artist rescue nil)
      base.parameterize
    end
end