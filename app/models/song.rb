class Song < ApplicationRecord
  belongs_to :album, optional: true
  belongs_to :artist, optional: true
  
  scope :popular, -> { order(:plays => :desc) }

  has_and_belongs_to_many :genres
  
  has_one_attached :image
  has_one_attached :audio_file
  
  validates_presence_of :image, if: :published
  
  include Soundcloud
  include Slug, Submittable

  
  validate :audio_file_or_soundcloud
  
  def audio_file_or_soundcloud
    unless audio_file.present? || soundcloud.present?
      errors.add(:audio_file, "Either audio file or Soundcloud link must be present")
    end
  end
  
  
  def cover
    image.presence || album&.cover&.presence
  end
end
