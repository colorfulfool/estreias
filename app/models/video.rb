class Video < ApplicationRecord
  belongs_to :artist, optional: true
  
  validates_presence_of :youtube
  
  default_scope { recent }
  
  include Youtube
  include Slug, Submittable
  
  def self.featured
    find(1)
  end
end
