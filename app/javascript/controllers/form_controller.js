import { Controller } from "stimulus"
const Rails = require("@rails/ujs")

export default class extends Controller {
  submit() {
    Rails.fire(this.element, "submit")
  }
}