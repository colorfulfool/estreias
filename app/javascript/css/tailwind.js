module.exports = {
  theme: {
    fontFamily: {
      'display': ['Futura', 'Nunito Sans', 'sans-serif'],
      'body': ['Work Sans', '-apple-system', 'sans-serif']
    },
    fontSize: {
      'xs': '14px',
      'sm': '16px',
      'base': '18px',
      'sub-lg': '21px',
      'lg': '24px',
      'xl': '36px',
      '2xl': '40px'
    },
    extend: {
      colors: {
        'gray-20': '#272C33', /* Medium jungle green */
        'gray-50': '#626D80', /* Dark electric blue */
        'gray-70': '#8999B3', /* Cool grey */
        'gray-100': '#B3D0FF', /* Pale cornflower blue */
        'white': '#E6EBF2', /* Glitter */
        'brand': '#FF3160', /* Radical Red */
        'alt-brand': '#CEFF31', /* Pear */

        'background': '#0D111A',
        'gray-6': '#090C12',
        'gray-3': '#05070a',
        'gray-11': 'hsl(219, 19%, 11%)'
      },
      borderWidth: {
        '1': '1px',
        '3': '3px',
        '5': '5px',
      },
      margin: {
        '13': '3.25rem'
      }
    },
    letterSpacing: {
      'wide': '1.8px',
      'wider': '2.4px',
      'widest': '3.67px'
    },
    boxShadow: {
      default: '0 5px 20px 0 rgba(0,0,0,0.28)',
      inset: 'inset 0 2px 20px 0 rgba(0,0,0,0.28)'
    },
    borderRadius: {
      default: '4px'
    }
  },
  variants: {
    margin: ['responsive', 'first', 'last', 'group-hover'],
    borderWidth: ['responsive', 'hover'],
    borderColor: ['hover', 'group-hover'],
    display: ['responsive', 'hover', 'group-hover'],
    backgroundColor: ['responsive', 'odd', 'focus-within', 'hover', 'group-hover'],
    textColor: ['hover', 'group-hover'],
  },
  plugins: []
}