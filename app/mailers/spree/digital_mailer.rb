class Spree::DigitalMailer < Spree::BaseMailer
  def download_link(order)
    @order = order
    subject = "#{Spree::Store.current.name}: #{@order.products.collect(&:name).to_sentence}"
    mail(to: @order.email, from: from_address, subject: subject)
  end
end
