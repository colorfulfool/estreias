class Spree::OrdersController < ApplicationController
  def create
    if @previous = Spree::Order.incomplete.find_by_token(cookies.signed[:token])
      @previous.destroy!
    end

    @order = Spree::Order.create!(
      store: spree_current_store,
      currency: current_currency,
      confirmation_delivered: true, # skip confirmation email
      token: Spree::GenerateToken.new.call(Spree::Order)
    )
    
    @order.line_items.create!(
      quantity: 1, 
      variant_id: params[:variant_id]
    )
    @order.update_with_updater!

    cookies.signed[:token] = @order.token
    cookies.permanent.signed[:guest_token] = @order.token
    
    redirect_to checkout_path
  end
  
  private
  
    def spree_current_store
      Spree::Store.last
    end
  
    def current_currency
      if spree_current_store.default_currency.present?
        spree_current_store.default_currency
      else
        Spree::Config[:currency]
      end
    end
end
