module Spree::CheckoutControllerDecorator
  private

    def completion_route(custom_params = nil)
      spree.root_path
    end
end

Spree::CheckoutController.prepend(Spree::CheckoutControllerDecorator)