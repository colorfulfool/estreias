class VideosController < ApplicationController
  def create
    @submission = Video.new(video_params)
    @submission.save
    render "submissions/create"
  end
  
  private
  
    def video_params
      params.require(:video).permit(:youtube)
    end
end
