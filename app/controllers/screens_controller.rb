class ScreensController < ApplicationController
  def show
    render params[:screen_name]
  end

  rescue_from ActionView::MissingTemplate do
    render file: "#{Rails.root}/public/404.html", status: 404, layout: nil
  end
end
