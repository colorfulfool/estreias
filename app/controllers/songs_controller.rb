class SongsController < ApplicationController
  before_action :set_song
  
  skip_before_action :verify_authenticity_token, 
    only: [:increment_plays, :increment_downloads]
    
  def show
    params[:screen_name] = "song"
    render "screens/song"
  end
  
  def increment_plays
    @song.increment! "plays"
  end
  
  def increment_downloads
    @song.increment! "downloads"
  end
  
  def create
    @submission = Song.new(song_params)
    @submission.save
    render "submissions/create"
  end
  
  private
  
    def set_song
      @song = Song.find_by_slug(params[:id])
    end
    
    def song_params
      params.require(:song).permit(:lyrics, :audio_file, :soundcloud)
    end
end
