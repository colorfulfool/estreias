# Spree::Auth::Config[:registration_step] = false
# didn't work, forking spree_auth_devise instead

Spree.config do |config|
  config.logo = "app/assets/images/logo.svg"
  
  config.products_per_page = 100

  config.track_inventory_levels = false
  config.address_requires_state = false
  
  config.default_country_id = 8 # Angola
end

Money.rounding_mode = BigDecimal::ROUND_HALF_UP
Money.default_currency = "USD"

Spree.user_class = "Spree::LegacyUser"
