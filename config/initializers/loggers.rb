# # log ShopifyAPI access
# ActiveResource::Base.logger = Rails.logger
#
# # but not request and response bodies
# class ActiveResource::DetailedLogSubscriber
#   def logger
#     nil
#   end
# end
#
# if Rails.env.production?
#   # don't log cache access in production
#   Rails.cache.silence!
# end

require 'silencer/logger'

Rails.application.configure do
  config.middleware.swap(
    Rails::Rack::Logger, 
    Silencer::Logger, 
    config.log_tags,
    silence: [
      %r{^/rails/active_storage/},
    ]
  )
end