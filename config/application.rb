require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Estreias
  class Application < Rails::Application

    config.to_prepare do
      # Load application's model / class decorators
      Dir.glob(File.join(File.dirname(__FILE__), "../app/**/*_decorator*.rb")) do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end

      # Load application's view overrides
      Dir.glob(File.join(File.dirname(__FILE__), "../app/overrides/*.rb")) do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end
    end
    
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    config.i18n.available_locales = [:en, :pt]
    
    Raven.configure do |config|
      config.dsn = 'https://93c18f20fc7b48c89fbef247a9a6148f:e1b9d3d37d784c7c91f3052b9243b9de@sentry.io/1780152'
      config.environments = ['production']
    end
    
    initializer 'spree.register.calculators' do |app|
      app.config.spree.calculators.shipping_methods = [Spree::BasicShippingCalculator]
    end
    
    initializer 'spree.register.payment_methods' do |app|
      app.config.spree.payment_methods = [Spree::BasicPaymentMethod]
    end
    
    initializer "spree.spree_digital.preferences" do |app|
      Spree::DigitalConfiguration[:authorized_days] = 30
    end
  end
end
