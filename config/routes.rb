Rails.application.routes.draw do
  mount Lit::Engine => '/lit'
  mount Spree::Core::Engine, at: '/shop'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  
  root "screens#show", screen_name: "home"
  
  resources "songs", only: [:show, :create] do
    member do
      post "increment_downloads"
      post "increment_plays"
    end
  end
  
  resources "videos", only: [:create]

  resolve "Song" do |record|
    [:song, :id => record.slug]
  end
  
  resources "submissions", only: [:create]
  
  Spree::Core::Engine.routes.draw do
    resources "orders", only: [:create]
  end
  
  resource "search", only: [:show]
  
  get "/:screen_name(/:id)", to: "screens#show", as: "screen"

  ["Album", "Artist", "Genre", "Movie", "Playlist", "Video"].each do |class_name|
    resolve class_name do |record|
      identifier = record.respond_to?(:slug) ? record.slug : record.id
      [:screen, :screen_name => class_name.downcase, :id => identifier]
    end    
  end
end
